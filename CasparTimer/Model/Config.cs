﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CasparTimer.Model
{
    public class Config
    {
        [XmlArray("Caspars")]
        [XmlArrayItem("Caspar")]
        public List<CasparInfo> CasparsInfo;

        [XmlArray("Listeners")]
        [XmlArrayItem("Listener")]
        public List<ListenerInfo> ListenerInfos;

        public Config()
        {
            CasparsInfo = new List<CasparInfo>();
            ListenerInfos = new List<ListenerInfo>();
        }   
    }
}
