﻿using System;

namespace CasparTimer.Model.Args
{
    public class TimeEventArgs : EventArgs
    {
        public int Seconds { get; }

        public TimeEventArgs(int seconds)
        {
            Seconds = seconds;
        }
    }
}
