﻿using CasparTimer.Model.Args;
using System;

namespace CasparTimer.Model.Interfaces
{
    public interface IListener : IDisposable
    {
        ListenerInfo Info { get; }

        event EventHandler<TimeEventArgs> TimeReceived;
        event EventHandler StartCommandReceived;        
    }
}
