﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CasparTimer.Model
{   
    public class ListenerInfo
    {       
        public int[] CasparsId
        {
            get => CasparsIdStrings.Split(';').Select(x => Convert.ToInt32(x.Trim())).ToArray();            
        }

        [XmlAttribute]
        public string Ip { get; set; }

        [XmlAttribute]
        public int Port { get; set; }

        [XmlAttribute]
        public Enums.ListenerType Type { get; set; }

        [XmlAttribute("CasparsID")]
        public string CasparsIdStrings { get; set; }
    }
}
