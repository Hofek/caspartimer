﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CasparTimer.Model
{  
    public class CasparInfo
    {       
        [XmlAttribute("ID")]
        public int Id { get; set; }

        [XmlAttribute("HostName")]
        public string HostName { get; set; }

        [XmlAttribute]
        public int Port { get; set; }        

        [XmlAttribute]
        public int Channel { get; set; }

        [XmlAttribute]
        public int Layer { get; set; }

        [XmlAttribute]
        public int FlashLayer { get; set; }        
        
        [XmlAttribute]
        public string FlashTemplate { get; set; }
    }
}
