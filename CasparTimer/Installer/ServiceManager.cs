﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CasparTimer.Installer
{
    public static class ServiceManager
    {
        private static string _serviceName = "CasparTimer";
        private static ServiceController _controller;
      

        public static bool IsInstalled()
        {
            _controller = ServiceController.GetServices().FirstOrDefault(p => p.ServiceName == _serviceName);

            if (_controller == null)
                return false;

            return true;
        }

        public static bool IsRunning()
        {
            _controller = ServiceController.GetServices().FirstOrDefault(p => p.ServiceName == _serviceName);

            if (_controller == null)
                return false;

            if (_controller.Status == ServiceControllerStatus.Running)
                return true;
            return false;
        }

        public static void Stop()
        {
            _controller = ServiceController.GetServices().FirstOrDefault(p => p.ServiceName == _serviceName);

            if (_controller == null)
                return;
            
            if (_controller.Status == ServiceControllerStatus.Stopped || _controller.Status == ServiceControllerStatus.StopPending)
                return;
            
            _controller.Stop();
            _controller.WaitForStatus(ServiceControllerStatus.Stopped);
        }

        public static void Start()
        {
            _controller = ServiceController.GetServices().FirstOrDefault(p => p.ServiceName == _serviceName);

            if (_controller == null)
                return;

            if (_controller.Status == ServiceControllerStatus.Running || _controller.Status == ServiceControllerStatus.StartPending)
                return;

            _controller.Start();
            _controller.WaitForStatus(ServiceControllerStatus.Running);
        }             
    }
}
