﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace CasparTimer.Installer
{
    [RunInstaller(true)]
    [DesignerCategory("Code")]
    internal class ServiceInstaller : System.Configuration.Install.Installer
    {
        private System.ServiceProcess.ServiceInstaller serviceInstaller;        
        private ServiceProcessInstaller processInstaller;

        public ServiceInstaller()
        {
            // Instantiate installers for process and services.
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new System.ServiceProcess.ServiceInstaller();
           

            // The services run under the system account.
            processInstaller.Account = ServiceAccount.LocalSystem;

            // The services are started manually.
            serviceInstaller.StartType = ServiceStartMode.Manual;            

            // ServiceName must equal those on ServiceBase derived classes.
            serviceInstaller.ServiceName = "CasparTimer";
            serviceInstaller.DisplayName = "CasparTimer";
            serviceInstaller.Description = "Service controlling timer flash template in CasparCG";

            // Add installers to collection. Order is not important.
            Installers.Add(serviceInstaller);            
            Installers.Add(processInstaller);
        }

       public bool Install()
       {
            var service = new ServiceInstaller();           

            if (!ServiceManager.IsInstalled())
            {
                var ctx = new InstallContext();
                service.Context = ctx;
                ctx.Parameters["assemblypath"] = Assembly.GetExecutingAssembly().Location;

                try
                {
                    service.Install(new Hashtable());                                                         
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            
            return false;
        }        

        public bool Uninstall()
        {
            var service = new ServiceInstaller();
           
            var ctx = new InstallContext();
            service.Context = ctx;
            ctx.Parameters["assemblypath"] = Assembly.GetExecutingAssembly().Location;

            if (ServiceManager.IsInstalled())
            {
                try
                {
                    ServiceManager.Stop();
                    service.Uninstall(null);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
    }
}
