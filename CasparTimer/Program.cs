﻿using CasparTimer.Model;
using CasparTimer.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CasparTimer
{
    static class Program
    {
        private static Config _config = null;
        private static CommunicationController _communicationController;
        private static SystemTickWatcher _systemTickWatcher;

        #region Nested classes to support running as service
        private const string ServiceName = "CasparTimer";        

        public class Service : ServiceBase
        {
            public Service()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
#if DEBUG
                System.Diagnostics.Debugger.Launch();
#endif
                Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
                Program.Start(args);

            }

            protected override void OnStop()
            {
                Program.Stop();
            }
        }
        #endregion

        static void Main(string[] args)
        {
            bool exit = false;
            if (!Environment.UserInteractive)
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
            {
                Start(args);

                if (_config == null)
                    Console.WriteLine("Configuration read failed.");

                while (!exit)
                {
                    Console.Write("> ");
                    string command = Console.ReadLine();

                    switch (command.ToLower())
                    {
                        case "quit":
                        case "q":
                            {
                                exit = true;
                                break;
                            }
                        case "install":
                            {
                                var serviceInstaller = new Installer.ServiceInstaller();

                                if (serviceInstaller.Install())
                                    Console.WriteLine("Service installed successfully");
                                else
                                    Console.WriteLine("Error. Try running again with admin rights. Check if service already exists.");

                                break;
                            }

                        case "uninstall":
                            {
                                var serviceInstaller = new Installer.ServiceInstaller();

                                if (serviceInstaller.Uninstall())
                                    Console.WriteLine("Service uninstalled successfully");
                                else
                                    Console.WriteLine("Error. Try running again with admin rights. Check if service already exists.");

                                break;
                            }

                        default:
                            {
                                var extendedCommand = command.Split(' ');

                                switch (extendedCommand[0])
                                {
                                    case "set":
                                        {
                                            if (extendedCommand.Length < 3 || !(Int32.TryParse(extendedCommand[1], out var casparId) && Int32.TryParse(extendedCommand[2], out var seconds)))
                                                break;

                                            _communicationController.SetTimerManually(casparId, seconds);
                                            break;
                                        }

                                    case "start":
                                        {
                                            if (extendedCommand.Length < 2 || !(Int32.TryParse(extendedCommand[1], out var casparId)))
                                                break;

                                            _communicationController.StartTimerManully(casparId);
                                            break;
                                        }

                                    case "hide":
                                    case "stop":
                                        {
                                            if (extendedCommand.Length > 2 && Int32.TryParse(extendedCommand[1], out var casparId) && extendedCommand[2] == "force")
                                            {
                                                _communicationController.StopTimerManually(casparId, true);
                                            }
                                            else if (extendedCommand.Length > 1 && (Int32.TryParse(extendedCommand[1], out casparId)))
                                            {
                                                _communicationController.StopTimerManually(casparId);
                                            }

                                            break;
                                        }

                                    default:
                                        break;
                                }
                                break;
                            }


                    }
                }
                Stop();
            }
        }


        private static void OnTicksOverflow(object sender, EventArgs e)
        {
            _communicationController.ReloadLocalCasparsTemplate();
        }

        private static void Start(string[] args)
        {
            if (!File.Exists("configuration.xml"))
                return;

            using (var reader = new StreamReader("configuration.xml"))
            {
                var serializer = new XmlSerializer(typeof(Config));
                _config = (Config)serializer.Deserialize(reader);
                foreach (var casparInfo in _config.CasparsInfo)
                    casparInfo.Channel -= 1;
            }

            _communicationController = new CommunicationController(_config);
            _systemTickWatcher = new SystemTickWatcher();
            _systemTickWatcher.TicksOverflow += OnTicksOverflow;
        }


        private static void Stop()
        {
            _communicationController?.Dispose();
            _systemTickWatcher?.Dispose();

            if (_systemTickWatcher == null)
                return;

            _systemTickWatcher.TicksOverflow -= OnTicksOverflow;
        }
    }
}
