﻿using CasparTimer.Model;
using CasparTimer.Model.Args;
using CasparTimer.Model.Interfaces;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace CasparTimer.Listeners
{
    public class VizListener : IListener
    {
        private TcpListener _server;
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private int _disposed;

        public event EventHandler<TimeEventArgs> TimeReceived;
        public event EventHandler StartCommandReceived;          

        public ListenerInfo Info { get; }

        public VizListener(ListenerInfo listenerInfo, string ip = "127.0.0.1")
        {
            Info = listenerInfo;

            var parsedIp = IPAddress.Parse(listenerInfo.Ip ?? ip);
            _server = new TcpListener(parsedIp, listenerInfo.Port);

            Listen();
        }

        private async void Listen()
        {
            Byte[] bytes = new Byte[256];
            String data = null;
            TcpClient client = null;
            NetworkStream stream = null;
            int bytesRead;

            _server.Start();

            while (true)
            {
                try
                {
                    if (_cancellationTokenSource.IsCancellationRequested)
                        throw new OperationCanceledException(_cancellationTokenSource.Token);

                    data = null;

                    Debug.WriteLine("[Listener] Waiting for client connection...");

                    client = await _server.AcceptTcpClientAsync().ConfigureAwait(false);
                    stream = client.GetStream();

                    Debug.WriteLine("[Listener] Client connected!");


                    while ((bytesRead = await stream.ReadAsync(bytes, 0, bytes.Length)) != 0)
                    {
                        data = System.Text.Encoding.ASCII.GetString(bytes, 0, bytesRead);
                        Debug.WriteLine("Received: {0}", data);

                        ParseCommand(data);
                    }
                }
                catch (Exception ex)
                {
                    client?.Close();
                    if (ex is OperationCanceledException)
                        break;
                    else if (ex is IOException)
                        Debug.WriteLine("Client disconnected");
                }
            }

            _server.Stop();
        }

        private void ParseCommand(string command)
        {
            if (command.Contains("TIME SET") && command.Contains("CLOCK"))
            {
                command = command.Remove(command.Length - 3, 3);

                if (!Int32.TryParse(command.Substring(command.LastIndexOf(' ')), out var seconds))
                    return;

                OnTimeReceived(new TimeEventArgs(seconds));
            }
            else if (command.Contains("START") && command.Contains("CLOCK"))
                OnStartCommandReceived();
        }

        private void OnTimeReceived(TimeEventArgs e)
        {
            TimeReceived?.Invoke(this, e);
        }

        private void OnStartCommandReceived()
        {
            StartCommandReceived?.Invoke(this, EventArgs.Empty);
        }

        public void Dispose()
        {
            if (Interlocked.Exchange(ref _disposed, 1) != default(int))
                return;

            _cancellationTokenSource.Cancel();
        }
    }
}
