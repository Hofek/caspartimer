﻿using Casparcg.Core.Device;
using Casparcg.Core.Network;
using CasparTimer.Model;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace CasparTimer.Services
{
    public class CasparCommunicator : IDisposable
    {
        private readonly CasparDevice _casparDevice = new CasparDevice();
        private int _disposed;
        private System.Timers.Timer _scheduleTimer;
        private System.Timers.Timer _playingTimer;
        private SemaphoreSlim _reloadSemaphore = new SemaphoreSlim(1);
        private bool _isTemplateRunning { get; set; }
        private bool _isTemplateLoaded { get; set; }
        private bool _isConnected { get; set; }

        public CasparInfo Info { get; }
                      
        public CasparCommunicator(CasparInfo casparInfo)
        {            
            Info = casparInfo;            
            _casparDevice.ConnectionStatusChanged += CasparDevice_ConnectionStatusChanged;
            _casparDevice.Settings.AutoConnect = false;

            Init();
        }

        private void CasparDevice_ConnectionStatusChanged(object sender, ConnectionEventArgs e)
        {
            if (e.Connected)
            {
                _isConnected = true;
                Debug.WriteLine("Caspar connected!");
                
                SetUpTimer();
            }
            else
            {
                _isConnected = false;
                Debug.WriteLine("Caspar disconnected.");

                if (_disposed == 1)
                    return;

                Debug.WriteLine("Reconnecting...");             
                Init();
            }
        }      

        private async void Init() // should be TASK, but ReSharper says otherwise
        {
            try
            {                
                _casparDevice.Connect(Info.HostName, Info.Port);
                if (!_casparDevice.IsConnected)
                {
                    await Task.Delay(500);
                    return;
                }
                                         
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }        

        private void SetUpTimer()
        {            
            DateTime targetDateTime;
            DateTime currentDateTime = DateTime.Now;
            int miliseconds;

            if (currentDateTime.TimeOfDay > new TimeSpan(5, 0, 0))
                targetDateTime = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day+1, 5, 0, 0);
            else
                targetDateTime = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, 5, 0, 0);
            
            miliseconds = (int)Math.Abs(currentDateTime.Subtract(targetDateTime).TotalMilliseconds);
            
            Debug.WriteLine(miliseconds.ToString());

            _scheduleTimer = new System.Timers.Timer(miliseconds);            
            _scheduleTimer.Elapsed += ScheduleTimer_Elapsed;
            _scheduleTimer.Start();
        }

        private void ScheduleTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            UnloadTemplate();
        }           
        
        public async void UnloadTemplate(bool force = false) // should be TASK, but ReSharper says otherwise
        {
            if (!_isConnected)
                return;

            if (_reloadSemaphore.CurrentCount == 0 && !force) //discard other requests if one is waiting
                return;

            while(true && !force)
            {                
                await _reloadSemaphore.WaitAsync(); //first is always going through, to signal that thread entered  

                if (_isTemplateRunning)
                    continue;
                
                break;
            }
                   
            _casparDevice.Channels[Info.Channel].CG.Clear(Info.Layer);
            
            _playingTimer?.Stop();
            _scheduleTimer?.Stop();
            _isTemplateRunning = false;
            SetUpTimer();

            _reloadSemaphore.Release(); //Restart semaphore
        }

        public void SetTemplateTimer(int seconds)
        {
            if (!_isConnected)
                return;

            if (seconds < 0)
                return;            
                       
            CasparCGDataCollection dataCollection = new CasparCGDataCollection();
            dataCollection.SetData("time", seconds.ToString());

            _casparDevice.Channels[Info.Channel].CG.Add(Info.Layer, Info.FlashLayer, Info.FlashTemplate, false, dataCollection);
            _isTemplateLoaded = true;

            _playingTimer = new System.Timers.Timer();
            _playingTimer.Interval = (seconds+1) * 1000;
            _playingTimer.Elapsed += PlayingTimer_Elapsed;           
        }

        private void PlayingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _playingTimer.Stop();
            _isTemplateRunning = false;
            if (_reloadSemaphore.CurrentCount == 0) // jezeli 0 -  watek czeka na zakoczenie odtwarzania
                _reloadSemaphore.Release();
        }

        public void StartTemplateTimer()
        {
            if (!_isConnected || !_isTemplateLoaded)
                return;            

            _casparDevice.Channels[Info.Channel].CG.Play(Info.Layer, Info.FlashLayer);
            _isTemplateRunning = true;
            _isTemplateLoaded = false;

            _playingTimer.Start();
        }

        public void Dispose()
        {
            if (Interlocked.Exchange(ref _disposed, 1) != default(int))
                return;
            _playingTimer?.Stop();
            _scheduleTimer?.Stop();

            _casparDevice.ConnectionStatusChanged -= CasparDevice_ConnectionStatusChanged;
            _casparDevice?.Disconnect();
            Debug.WriteLine("CasparCommunicator disposed");
        }
    }
}
