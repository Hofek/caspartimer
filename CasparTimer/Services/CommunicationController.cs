﻿using CasparTimer.Listeners;
using CasparTimer.Model;
using CasparTimer.Model.Args;
using CasparTimer.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace CasparTimer.Services
{
    public class CommunicationController : IDisposable
    {
        private readonly List<IListener> _listeners;
        private readonly List<CasparCommunicator> _casparCommunicators;
        private int _disposed;

        public CommunicationController(Config _config)
        {
            _listeners = new List<IListener>();
            _casparCommunicators = new List<CasparCommunicator>();

            InitListeners(_config.ListenerInfos);
            InitCasparCommunicators(_config.CasparsInfo);
        }

        private void InitListeners(IList<ListenerInfo> listenersInfo)
        {
            foreach(var listenerInfo in listenersInfo)
            {
                switch(listenerInfo.Type)
                {
                    case Enums.ListenerType.viz:
                        {
                            if (_listeners.Any(l => l.Info.Port == listenerInfo.Port))
                                continue;

                            var listener = new VizListener(listenerInfo);
                            listener.TimeReceived += OnTimeReceived;
                            listener.StartCommandReceived += OnStartCommandReceived;
                            _listeners.Add(listener);

                            Debug.WriteLine($"Listener created. IP: {listener.Info.Ip} PORT: {listener.Info.Port}");
                            break;
                        }
                }
            }
        }       

        private void InitCasparCommunicators(IList<CasparInfo> casparsInfo)
        {
            foreach(var casparInfo in casparsInfo)
            {
                if (_casparCommunicators.Any(c => c.Info.HostName == casparInfo.HostName && c.Info.Port == casparInfo.Port))
                    return;

                _casparCommunicators.Add(new CasparCommunicator(casparInfo));
                Debug.WriteLine($"Caspar communicator created. HOSTNAME: {casparInfo.HostName} PORT: {casparInfo.Port}");
            }
        }

        private void OnStartCommandReceived(object sender, EventArgs e)
        {
            var listener = sender as IListener;
            foreach (var id in listener.Info.CasparsId)
            {
                var casparCommunicator = _casparCommunicators.FirstOrDefault(c => c.Info.Id == id);

                if (casparCommunicator == null)
                    continue;

                casparCommunicator.StartTemplateTimer();
            }
        }

        private void OnTimeReceived(object sender, TimeEventArgs e)
        {
            var listener = sender as IListener;
            foreach(var id in listener.Info.CasparsId)
            {
                var casparCommunicator = _casparCommunicators.FirstOrDefault(c => c.Info.Id == id);
                
                if (casparCommunicator == null)
                    continue;

                casparCommunicator.SetTemplateTimer(e.Seconds);
            }
        }

        public void SetTimerManually(int casparId, int seconds)
        {
            var casparCommunicator = _casparCommunicators.FirstOrDefault(c => c.Info.Id == casparId);

            if (casparCommunicator == null)
                return;

            casparCommunicator.SetTemplateTimer(seconds);
        }

        public void StartTimerManully(int casparId)
        {
            var casparCommunicator = _casparCommunicators.FirstOrDefault(c => c.Info.Id == casparId);

            if (casparCommunicator == null)
                return;

            casparCommunicator.StartTemplateTimer();
        }

        public void StopTimerManually(int casparId, bool force = false)
        {
            var casparCommunicator = _casparCommunicators.FirstOrDefault(c => c.Info.Id == casparId);

            if (casparCommunicator == null)
                return;

            casparCommunicator.UnloadTemplate(force);
        }

        public void ReloadLocalCasparsTemplate()
        {
            var casparCommunicators = _casparCommunicators.Where(c => c.Info.HostName == "127.0.0.1" || c.Info.HostName.ToLower() == "localhost");

            foreach (var casparCommunicator in casparCommunicators)
                casparCommunicator.UnloadTemplate(true);
        }

        public void Dispose()
        {
            if (Interlocked.Exchange(ref _disposed, 1) != default(int))
                return;

            foreach (var listener in _listeners)
            {
                listener.TimeReceived -= OnTimeReceived;
                listener.StartCommandReceived -= OnStartCommandReceived;
                listener.Dispose();
            }

            foreach(var communicator in _casparCommunicators)
                communicator.Dispose();
            
            Debug.WriteLine("CommunicationController disposed");
        }
    }
}
