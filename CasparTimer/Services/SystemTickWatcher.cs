﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CasparTimer.Services
{
    //patch for Flash problems when Environment.TickCount variable overflows
    public class SystemTickWatcher : IDisposable
    {
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
                
        public SystemTickWatcher()
        {            
            InitWatcher();
        }

        public event EventHandler TicksOverflow;       

        private async void InitWatcher() //should be TASK, but ReSharper says otherwise...
        {
            try
            {
                int oldTicks = Environment.TickCount;

                while (true)
                {
                    if (_cancellationTokenSource.IsCancellationRequested)
                        throw new OperationCanceledException(_cancellationTokenSource.Token);
                    int newTicks = Environment.TickCount;

                    if (oldTicks<0 && newTicks>=0)
                    {
                        TicksOverflow?.Invoke(this, EventArgs.Empty);
                        oldTicks = newTicks;
                    }

                    await Task.Delay(10000);
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
            _cancellationTokenSource?.Cancel();
            Debug.WriteLine("SystemTickWatcher disposed");
        }
    }
}
