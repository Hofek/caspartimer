package src 
{
	/**
	 * ...
	 * @author Jerzy Jaśkiewicz
	 */
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import se.svt.caspar.template.CasparTemplate;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Shape;
	
	[SWF(width=1920, height=1080, frameRate="50")]
	public class Main extends CasparTemplate
	{
		
		private var timer:CountdownTimer = null;
		
		public function Main():void 
		{			
			this.originalFrameRate = 50;
			this.originalWidth = 1920;
			this.originalHeight = 1080;
			this.description = new XML(
				 <template version="1.0.0" authorName="Jerzy Jaśkiewicz" authorEmail="jurek@tvp.pl" templateInfo="CountdownTimer" originalWidth="1920" originalHeight="1080" originalFrameRate="50" >
				 	<components>
					</components>
					<keyframes></keyframes>
					<parameters>
					</parameters>
				</template>			
			);
			this.addEventListener(Event.ADDED_TO_STAGE, addedToStageEventHandler);			
		}
		
		override public function SetData(xmlData:XML):void 
		{			
			var seconds:int = xmlData.componentData.(@id == "time").data.@value;
			
			if (seconds >= 0)
			{				
				if (timer && contains(timer))
					removeChild(timer);
					
				timer = new CountdownTimer(seconds, originalFrameRate);	
				timer.addEventListener("TimerFinished", OnTimerFinished);
				addChild(timer);						
			}
			super.SetData(xmlData);			
		}
		
		private function OnTimerFinished(event:Event):void
		{
			removeChild(timer);
			trace("Template unloaded");
			super.removeTemplate();					
		}
		
		private function addedToStageEventHandler(ev:Event):void
		{
			//TraceToLog("Added to stage");
			removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
			
			// entry point
			stage.addEventListener(MouseEvent.CLICK, function():void
				{
					TraceToLog("DataLoaded");
					SetData(new XML(
				<templateData>
					<componentData id="time">
						<data id="text" value="5"></data> 
					</componentData>
				</templateData>
				));
				});
			stage.addEventListener(MouseEvent.RIGHT_CLICK, function():void
				{
					Play();
				});
		}
		
		
		override public function Play():void
		{
			if (timer)
				timer.Start();
			super.Play();
		}
		

		override public function Stop():void
		{
			super.Stop();
		}

		
		override public function TraceToLog(message:String):void 
		{
			super.TraceToLog(message);
			trace(message);
		}			
	}
}