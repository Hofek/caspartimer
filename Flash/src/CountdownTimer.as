package src
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	import flash.system.System;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.globalization.DateTimeFormatter;
	import se.svt.caspar.template.CasparTemplate;
	import flash.text.GridFitType;
	import mx.utils.StringUtil;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	/**
	 * ...
	 * @author Jerzy Jaśkiewicz
	 */
	public class CountdownTimer extends Sprite
	{
		
		[Embed(source="../assets/0522EU26.ttf", fontFamily="Geometric415MdEU", fontWeight = FontWeight.NORMAL, fontStyle="regular", embedAsCFF = "false")]
		private var _fontGeometric415MdEU:Class;
		[Embed(source="../assets/background.png")]
		private var backgroundClass:Class;
		
		
		private var _background:Bitmap;
		private var _tf:TextField;
		private var _framesLeft:int;
		private var _fps:int;
		private var _stopped:Boolean;
		
		private var dtf:DateTimeFormatter = new DateTimeFormatter("pl-PL");		
		
		public function CountdownTimer(initialTime:int, fps:int) 
		{			
			alpha = 0;
			_fps = fps;
			_framesLeft = initialTime * fps;
			_background = new backgroundClass();
			addChild(_background);
			_tf = new TextField(); 
			//_tf.antiAliasType = AntiAliasType.ADVANCED; // usuniete, bo problemy z przezroczystością https://stackoverflow.com/questions/1481464/as3-tweener-antialias-and-alpha-problem
			_tf.autoSize = TextFieldAutoSize.CENTER;
			_tf.x = 1692;
			_tf.y = 77;
			_tf.background = false;
			_tf.embedFonts = true;
			_tf.multiline = false;
			_tf.gridFitType = GridFitType.NONE;
			
			//set the format for the font
			_tf.defaultTextFormat = new TextFormat("Geometric415MdEU", 47, 0xF0F0F0);
			
			//add to the display list
			this.addChild(_tf);							
				
		}
		
		private function Update(e:Event):void
		{
			var seconds:int = (_framesLeft / _fps);
			var minutes:int = (_framesLeft / _fps) / 60;
						
			dtf.setDateTimePattern("mm:ss");
			_tf.text = dtf.format(new Date(seconds*1000));
			
			if (_framesLeft > 0)
				_framesLeft--;
			else
				Stop();
		}
		
		public function Start():void
		{					
			removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
			addEventListener(Event.ENTER_FRAME, Update); // run on every frame
			addEventListener(Event.REMOVED_FROM_STAGE, 
				function(e:Event):void {
					e.currentTarget.removeEventListener(e.type, arguments.callee);
					removeEventListener(Event.ENTER_FRAME, Update); 
					trace("clock cleared");
				});
				
						
			TweenLite.to(this, 1, {alpha:1});	
			trace("started");
		}		
		
		public function Stop():void
		{
			if (_stopped)
				return;
			
			_stopped = true;
			trace("Obecny!");
			TweenLite.to(this, 1, {alpha:0, onComplete:unloadTemplate});	
			
			
		}		
		
		private function unloadTemplate():void
		{
			trace("Firing event");
			dispatchEvent(new Event("TimerFinished"));
		}
	}

}